package com.rzx.action;

import android.content.Context;
import android.content.Intent;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/20.
 */
public class InputAction extends Action{

    private String text;

    public InputAction(Context context, Connection connection, long taskId, int type, String text){
        super(context, connection, taskId, type);
        this.text=text;
    }

    @Override
    public void run() {
        context.sendBroadcast(new Intent("com.rzx.mspservice.input").putExtra("content", text));
        response("");
    }
}
