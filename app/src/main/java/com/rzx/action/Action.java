package com.rzx.action;

import android.content.Context;
import android.util.Log;

import com.rzx.monitor.StatusMonitor;
import com.rzx.mspservice.Connection;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/1/16.
 */
public abstract class Action implements Runnable {
    protected Context context;
    protected Connection connection;
    protected long taskId;
    protected int type;

    public Action(Context context, Connection connection, long taskId, int type){
        this.context=context;
        this.connection=connection;
        this.taskId=taskId;
        this.type=type;

    }

    protected long getTaskId(){
        return taskId;
    }

    protected int getType(){
        return type;
    }

    protected  String toJson(Object... objects){
        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("id", taskId);
            jsonObject.put("type", type);
            jsonObject.put("ret_code", objects[0]);
            jsonObject.put("ret_msg",objects[1]);
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
        return jsonObject.toString();
    }

    protected void response(String result){
        StatusMonitor.setStatus(StatusMonitor.STATUS_IDLE);
        if(result==null || result.trim().equals("")) return;
        connection.onResponse(result);
    }

}
