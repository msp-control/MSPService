package com.rzx.action;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;

import com.rzx.mspservice.Connection;

import org.json.JSONArray;

import java.io.File;

/**
 * Created by Administrator on 2017/2/21.
 */
public class RefreshPicturesAction extends Action{

    private JSONArray files;

    public RefreshPicturesAction(Context context, Connection connection, long taskId, int type, JSONArray files){
        super(context, connection, taskId, type);
        this.files=files;
    }

    @Override
    public void run() {

        if(files==null){
            response(toJson(-1, "Files args is null"));
            return;
        }

        try{
            String[] filepaths=new String[files.length()];
            for(int i=0; i<files.length(); i++){
                filepaths[i]=files.getString(i);
            }
            MediaScannerConnection.scanFile(context, filepaths, null, new MediaScannerConnection.OnScanCompletedListener() {
                @Override
                public void onScanCompleted(String path, Uri uri) {
                    response(toJson(0, "Refresh pictures succeed"));
                }
            });
        }catch (Exception e){
            response(toJson(-1, "Refresh pictures failed"));
        }

//        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(f)));
//        response(toJson(0, "Refresh pictures succeed"));
    }
}
