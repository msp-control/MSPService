package com.rzx.action;

import android.content.Context;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

/**
 * Created by Administrator on 2017/3/6.
 */
public class SuAction extends Action{

    private String cmd;

    public SuAction(Context context, Connection connection, long taskId, int type, String cmd){
        super(context, connection, taskId, type);
        this.cmd=cmd;
    }

    @Override
    public void run() {
        String result=CmdExecutor.executeWithResponse(cmd);
        response(toJson(0, result));
    }
}
