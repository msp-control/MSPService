package com.rzx.action;

import android.content.Context;
import android.view.KeyEvent;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

/**
 * Created by Administrator on 2017/2/10.
 */
public class KeyCodeHomeAction extends Action{

    public KeyCodeHomeAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        CmdExecutor.execute("input keyevent 3");

        response("");
    }
}
