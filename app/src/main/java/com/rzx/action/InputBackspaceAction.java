package com.rzx.action;

import android.content.Context;
import android.content.Intent;
import android.renderscript.ScriptGroup;
import android.view.KeyEvent;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/20.
 */
public class InputBackspaceAction extends Action{

    public InputBackspaceAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        context.sendBroadcast(new Intent("com.rzx.mspservice.input").putExtra("content", "\b"));
        response("");
    }
}
