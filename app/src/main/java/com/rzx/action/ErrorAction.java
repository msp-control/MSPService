package com.rzx.action;

import android.content.Context;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/8.
 */
public class ErrorAction extends Action {

    public ErrorAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        response(toJson(-1, "Bad request"));
    }

}
