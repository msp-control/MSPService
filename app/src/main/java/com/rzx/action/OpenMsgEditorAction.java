package com.rzx.action;

import android.content.Context;
import android.content.Intent;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/14.
 */
public class OpenMsgEditorAction extends Action{

    public OpenMsgEditorAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        Intent intent=new Intent(Intent.ACTION_VIEW);
        intent.setType("vnd.android-dir/mms-sms");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        response(toJson(0, "Open msg editor succeed"));
    }
}
