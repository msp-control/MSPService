package com.rzx.action;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.util.Log;

import com.rzx.mspservice.Connection;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/8.
 */
public class ClearContactsAction extends Action{

    public ClearContactsAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        ContentResolver cr = context.getContentResolver();

        cr.delete(ContactsContract.RawContacts.CONTENT_URI.buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,"true")
                .build(), "_id!=-1", null);

        response(toJson(0, "Contacts delete succeed"));

        //跳转到联系人页面
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setData(Contacts.People.CONTENT_URI);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
