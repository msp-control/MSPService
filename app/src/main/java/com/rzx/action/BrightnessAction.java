package com.rzx.action;

import android.content.Context;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/4.
 */
public class BrightnessAction extends Action {

    private int brightness;

    public BrightnessAction(Context context, Connection connection, long taskId, int type, int brightness){
        super(context, connection, taskId, type);
        this.brightness=brightness;
    }

    @Override
    public void run() {
        StringBuffer sb=new StringBuffer("settings put system screen_brightness ");
        sb.append(brightness);
        CmdExecutor.execute(sb.toString());

        String msg="Screen brightness adjustment succeed";
        response(toJson(0, msg));
    }

}
