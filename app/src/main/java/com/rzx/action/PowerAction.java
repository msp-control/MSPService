package com.rzx.action;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/8.
 */
public class PowerAction extends Action{

    private boolean enable;

    public PowerAction(Context context, Connection connection, long taskId, int type, boolean enable){
        super(context, connection, taskId, type);
        this.enable=enable;
    }

    @Override
    public void run() {
        String cmd=null;
        PowerManager pm=(PowerManager)context.getSystemService(Context.POWER_SERVICE);

        if(enable){
            if(!pm.isScreenOn()){
                cmd="input keyevent 26";
            }
        }else{
            if(pm.isScreenOn()){
                cmd="input keyevent 26";
            }
        }
        if(cmd!=null){
            CmdExecutor.execute(cmd);
        }

        String msg="";
        if(pm.isScreenOn()){
            msg="Power on";
        }else{
            msg="Power off";
        }
        response(toJson(0, msg));
    }

}
