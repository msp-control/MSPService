package com.rzx.action;

import android.content.Context;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/8.
 */
public class UninstallAction extends Action{

    private String packageName;

    public UninstallAction(Context context, Connection connection, long taskId, int type, String packageName){
        super(context, connection, taskId, type);
        this.packageName=packageName;
    }

    @Override
    public void run() {
        String msg="";
        if(packageName==null){
            msg="Package name is null";
            response(toJson(-1, msg));
            return;
        }

        StringBuffer sb=new StringBuffer("pm uninstall ");
        sb.append(packageName);
        String result= CmdExecutor.executeWithResponse(sb.toString());

        if(result.equals("Success")){
            msg="Uninstall succeed";
            response(toJson(0, msg));
        }else{
            response(toJson(-1, result));
        }

    }

}
