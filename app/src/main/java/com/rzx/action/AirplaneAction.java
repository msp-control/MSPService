package com.rzx.action;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/3.
 */
public class AirplaneAction extends Action {

    private boolean enable;

    public AirplaneAction(Context context, Connection connection, long taskId, int type, boolean enable){
        super(context, connection, taskId, type);
        this.enable=enable;
    }

    @Override
    public void run() {
        String[] cmds=new String[2];
        if(enable){
            cmds[0]="settings put global airplane_mode_on 1";
            cmds[1]="am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true";
        }else{
            cmds[0]="settings put global airplane_mode_on 0";
            cmds[1]="am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false";
        }
        CmdExecutor.execute(cmds);
        String airplaneState=Settings.System.getString(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON);
        String msg="";
        if(airplaneState.equals("0")){
            msg="Airplane mode is disabled";
        }else{
            msg="Airplane mode is enabled";
        }
        response(toJson(0, msg));
    }

}
