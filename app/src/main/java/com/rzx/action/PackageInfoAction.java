package com.rzx.action;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.rzx.mspservice.Connection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Administrator on 2017/2/13.
 */
public class PackageInfoAction extends Action{

    public PackageInfoAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {

        try{
            JSONArray jsonArray=new JSONArray();
            PackageManager pm=context.getPackageManager();
            List<PackageInfo> packageInfos=pm.getInstalledPackages(0);
            for(PackageInfo info : packageInfos){
                if((info.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM)>0) continue;
                JSONObject json=new JSONObject();
                json.put("name", info.applicationInfo.loadLabel(pm).toString());
                json.put("pkg", info.packageName);
                json.put("ver", info.versionName);
                jsonArray.put(json);
            }

            response(toJson(0, jsonArray));

        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());

            response(toJson(-1, "Get installed package info failed"));
        }

    }
}
