package com.rzx.action;

import android.content.Context;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/8.
 */
public class RebootAction extends Action{

    public RebootAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        String msg="Ready to reboot";
        response(toJson(0, msg));
        CmdExecutor.execute("reboot");
    }

}
