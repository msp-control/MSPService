package com.rzx.action;

import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import com.rzx.mspservice.Connection;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created by Administrator on 2017/2/9.
 */
public class SendMsgAction extends Action{

    private String number;
    private String msg;

    public SendMsgAction(Context context, Connection connection, long taskId, int type, String number, String msg){
        super(context, connection, taskId, type);
        this.number=number;
        this.msg=msg;
    }

    @Override
    public void run() {

        if(msg==null || number==null){
            response(toJson(-1, "Number or msg is null"));
            return;
        }

        try{
            SmsManager sm = SmsManager.getDefault();
            ArrayList<String> list = sm.divideMessage(msg);  //因为一条短信有字数限制，因此要将长短信拆分
            for(String text:list){
                sm.sendTextMessage(number, null, text, null, null);
            }
        }catch (SecurityException e){
            response(toJson(-1, "Send msg failed"));
            return;
        }

        response(toJson(0, "Send msg succeed"));
    }

}
