package com.rzx.action;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/8.
 */
public class InstallAction extends Action{

    private String filepath;

    public InstallAction(Context context, Connection connection, long taskId, int type, String filepath){
        super(context, connection, taskId, type);
        this.filepath=filepath;
    }

    @Override
    public void run() {
        String msg="";
        if(filepath==null){
            msg="Filepath is null";
            response(toJson(-1, msg));
            return;
        }
        File f=new File(filepath);
        if(!f.exists()){
            msg="File is not exist";
            response(toJson(-1, msg));
            return;
        }
        StringBuffer sb=new StringBuffer("pm install -r ");
        sb.append(filepath);
        String result=CmdExecutor.executeWithResponse(sb.toString());
        Log.d("msp", "install result:" + result);

        String[] rets = result.split("\n");

        if(rets[rets.length - 1].equals("Success")){
            msg="Install succeed";
            response(toJson(0, msg));

            //安装完后删除
            CmdExecutor.execute("rm " + filepath);
        }else{
            msg=rets[rets.length - 1];
            response(toJson(-1, msg));
        }

    }
}
