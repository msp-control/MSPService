package com.rzx.action;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.util.Log;

import com.rzx.mspservice.Connection;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/2/8.
 */
public class ImportContactsAction extends Action{

    private String filepath;

    public ImportContactsAction(Context context, Connection connection, long taskId, int type, String filepath){
        super(context, connection, taskId, type);
        this.filepath=filepath;
    }

    @Override
    public void run() {
        String msg="";
        if(filepath==null){
            msg="Filepath is null";
            response(toJson(-1, msg));
            return;
        }
        File f=new File(filepath);
        if(!f.exists()){
            msg="File is not exist";
            response(toJson(-1, msg));
            return;
        }
        ArrayList contacts=new ArrayList();
        BufferedReader br=null;
        try{
            br=new BufferedReader(new FileReader(f));
            String line;
            while((line=br.readLine())!=null){
                String[] contactInfo=line.split(" ");
                Contact contact=new Contact(contactInfo[0], contactInfo[1]);
                contacts.add(contact);
            }
            addContactBatch(contacts);
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }finally {
            if(br!=null){
                try {
                    br.close();
                }catch (Exception e){

                }
            }
        }


        msg="Import contacts succeed";
        response(toJson(0, msg));


        //跳转到联系人页面
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setData(Contacts.People.CONTENT_URI);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public void addContactBatch(List<Contact> contacts)
            throws RemoteException, OperationApplicationException {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactInsertIndex;
        for (Contact contact : contacts) {
            rawContactInsertIndex = ops.size();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .withYieldAllowed(true).build());

            // 添加姓名
            ops.add(ContentProviderOperation
                    .newInsert(
                            android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                            rawContactInsertIndex)
                    .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getName())
                    .withYieldAllowed(true).build());
            // 添加号码
            ops.add(ContentProviderOperation
                    .newInsert(
                            android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                            rawContactInsertIndex)
                    .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, contact.getNumber())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.LABEL, "").withYieldAllowed(true).build());
        }
        if (ops != null) {
            // 批量添加
            context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        }
    }

    private class Contact{
        private String name;
        private String number;

        public Contact(String name, String number){
            this.name=name;
            this.number=number;
        }

        public String getName() {
            return name;
        }

        public String getNumber() {
            return number;
        }
    }
}
