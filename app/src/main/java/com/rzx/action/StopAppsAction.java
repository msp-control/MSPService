package com.rzx.action;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/2/9.
 */
public class StopAppsAction extends Action{

    public StopAppsAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {

        //获取第三方应用包名
        List<String> packagenames=new ArrayList<>();
        PackageManager pm=context.getPackageManager();
        List<PackageInfo> packageInfos=pm.getInstalledPackages(0);
        for(PackageInfo info : packageInfos){
            if((info.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM)>0) continue;
            if(info.packageName.equals("com.rzx.mspservice")) continue;
            packagenames.add(info.packageName);
        }

        //停止所有三方应用
        for(String packagename : packagenames){
            StringBuffer sb=new StringBuffer("am force-stop ");
            sb.append(packagename);
            CmdExecutor.execute(sb.toString());
        }

        response(toJson(0, "Stop other applications succeed"));

    }
}
