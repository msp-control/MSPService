package com.rzx.action;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/20.
 */
public class InputEnterAction extends Action{

    public InputEnterAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        context.sendBroadcast(new Intent("com.rzx.mspservice.input").putExtra("content", "\n"));
        response("");
    }
}
