package com.rzx.action;

import android.content.Context;
import android.content.Intent;

import com.rzx.mspservice.Connection;
import com.rzx.util.ClassParseUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/2/17.
 */
public class SetLocationAction extends Action{

    private String location;
    private JSONArray positionsArray;

    public SetLocationAction(Context context, Connection connection, long taskId, int type, String location, JSONArray positions){
        super(context, connection, taskId, type);
        this.location=location;
        positionsArray=positions;
    }

    @Override
    public void run() {

        if(positionsArray==null){
            response(toJson(-1, "Location args is null"));
            return;
        }

        try{
            List<Map<String, String>> positions=new ArrayList<>();
            for(int i=0; i<positionsArray.length(); i++){
                JSONObject positionJson=positionsArray.getJSONObject(i);
                String lat=positionJson.getString("lat");
                String lng=positionJson.getString("lng");
                HashMap position=new HashMap();
                position.put("lat", lat);
                position.put("lng", lng);
                positions.add(position);
            }
            String positionsStr=ClassParseUtil.list2String(positions);
            context.getSharedPreferences("cache", Context.MODE_PRIVATE).edit().putString("positions", positionsStr).commit();
            context.sendBroadcast(new Intent("com.rzx.mspservice.location.update"));
            response(toJson(0, "Location update succeed"));
            return;
        }catch (Exception e){

        }
        response(toJson(-1, "Location update failed"));

//        PropertiesUtil.setProperties("/sdcard/msp/res/location.properties", "lat", String.valueOf(lat));
//        PropertiesUtil.setProperties("/sdcard/msp/res/location.properties", "lng", String.valueOf(lng));

    }
}
