package com.rzx.action;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/1/16.
 */
public class WifiAction extends Action {

    private boolean enable;

    public WifiAction(Context context, Connection connection, long taskId, int type, boolean enable){
        super(context, connection, taskId, type);
        this.enable=enable;
    }

    @Override
    public void run() {
        String cmd="";
        if(enable){
            cmd="svc wifi enable";
        }else{
            cmd="svc wifi disable";
        }
        CmdExecutor.execute(cmd);

        WifiManager wm=(WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        int wifiState=wm.getWifiState();
        String msg="Wifi operation is finished";
        //wifi开启关闭有延迟，不适用
//        switch (wifiState){
//            case WifiManager.WIFI_STATE_DISABLING:
//                msg="wifi is disabling";
//                break;
//            case WifiManager.WIFI_STATE_DISABLED:
//                msg="wifi is disabled";
//                break;
//            case WifiManager.WIFI_STATE_ENABLING:
//                msg="wifi is enabling";
//                break;
//            case WifiManager.WIFI_STATE_ENABLED:
//                msg="wifi is enabled";
//                break;
//        }
        response(toJson(0, msg));
    }

}
