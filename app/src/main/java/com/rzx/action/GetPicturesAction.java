package com.rzx.action;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

import com.rzx.mspservice.Connection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/21.
 */
public class GetPicturesAction extends Action{

    public GetPicturesAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {

        try{
            ContentResolver cr=context.getContentResolver();
            Cursor cursor=cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATA
            }, null, null, null);

            if(cursor!=null){
                JSONArray array=new JSONArray();
                while(cursor.moveToNext()){
                    JSONObject picture=new JSONObject();
                    String name=cursor.getString(0);
                    String path=cursor.getString(1);
                    picture.put("display_name", name);
                    picture.put("data", path);
                    array.put(picture);
                }
                cursor.close();

                response(toJson(0, array));
                return;
            }
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }

        response(toJson(-1, "Get pictures info failed"));
    }
}
