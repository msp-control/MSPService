package com.rzx.action;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.MediaStore;
import android.util.Log;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/14.
 */
public class ClearPicturesAction extends Action{

    public ClearPicturesAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        ContentResolver cr=context.getContentResolver();
        cr.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "_id!=-1", null);

        response(toJson(0, "Clear pictures succeed"));

    }
}
