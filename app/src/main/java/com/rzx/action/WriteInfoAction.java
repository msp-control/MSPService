package com.rzx.action;

import android.content.Context;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;
import com.rzx.util.PropertiesUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Administrator on 2017/2/21.
 */
public class WriteInfoAction extends Action{

    private JSONObject info;

    public WriteInfoAction(Context context, Connection connection, long taskId, int type, JSONObject info){
        super(context, connection, taskId, type);
        this.info=info;
    }

    @Override
    public void run() {

        try {
            if(info.has("virtualIdentityInfo")){
                JSONObject identity=info.getJSONObject("virtualIdentityInfo");
                String id=String.valueOf(identity.getInt("id"));
                String firstname=identity.optString("first_name");
                String lastname=identity.optString("last_name");
                String gender=identity.optString("gender");
                String age=identity.optString("age");
                String work=identity.optString("work");
                String hobby=identity.optString("hobby");
                String phoneNumber=identity.optString("phonenumber");
                String ips=identity.optString("IPs");
                String idcard=identity.optString("idcard");

                JSONObject device=identity.getJSONObject("virtualDeviceInfo");
                Map<String, String> deviceInfo=new HashMap<>();
                String model=device.optString("model");
                String serial=device.optString("serial");
                String imei=device.optString("imei");
                String imsi=device.optString("imsi");
                String mac=device.optString("mac");

                String android_id = device.optString("android_id");
                if (android_id != "") {
                    CmdExecutor.execute("settings put secure android_id " + android_id);
                }
                if (model != ""){
                    deviceInfo.put("model", model);
                }
                if (serial != ""){
                    deviceInfo.put("serial", serial);
                }
                if (imei != ""){
                    deviceInfo.put("imei", imei);
                }
                if (imsi != ""){
                    deviceInfo.put("imsi", imsi);
                }
                if (mac != ""){
                    deviceInfo.put("mac", mac);
                }

                PropertiesUtil.setProperties("/sdcard/msp/res/virtualDeviceInfo.properties", deviceInfo);
                response(toJson(0, "Save info succeed"));
                return;
            }
        }catch (Exception e){
            response(toJson(-1, "Save info failed: "+e.toString()));
            return;
        }

        response(toJson(-1, "Save info failed"));
    }
}
