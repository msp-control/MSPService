package com.rzx.action;

import android.content.Context;

import com.rzx.mspservice.Connection;
import com.rzx.util.CmdExecutor;

/**
 * Created by Administrator on 2017/2/10.
 */
public class KeyCodeAction extends Action{

    private int keycode;

    public KeyCodeAction(Context context, Connection connection, long taskId, int type, int keycode){
        super(context, connection, taskId, type);
        this.keycode=keycode;
    }

    @Override
    public void run() {
        StringBuffer sb=new StringBuffer("input keyevent ");
        sb.append(keycode);
        CmdExecutor.execute(sb.toString());
        
        response("");
    }
}
