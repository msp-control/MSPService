package com.rzx.action;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Contacts;
import android.provider.ContactsContract;

import com.rzx.mspservice.Connection;

/**
 * Created by Administrator on 2017/2/20.
 */
public class DeleteContactAction extends Action {

    private long contactId;

    public DeleteContactAction(Context context, Connection connection, long taskId, int type, long contactId){
        super(context, connection, taskId, type);
        this.contactId=contactId;
    }

    @Override
    public void run() {
        ContentResolver cr=context.getContentResolver();
        cr.delete(ContactsContract.RawContacts.CONTENT_URI, ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"="+contactId,
                null);


        response(toJson(0, "Delete contact succeed"));

        //跳转到联系人页面
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setData(Contacts.People.CONTENT_URI);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
