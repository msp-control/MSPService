package com.rzx.action;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.rzx.mspservice.Connection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/20.
 */
public class GetContactsListAction  extends Action{

    public GetContactsListAction(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        try{
            ContentResolver cr=context.getContentResolver();
            Cursor cursor=cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.CommonDataKinds.Phone.NUMBER,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID}, null, null, null);

            if(cursor!=null){
                JSONArray array=new JSONArray();
                while(cursor.moveToNext()){
                    JSONObject contact=new JSONObject();
                    String name=cursor.getString(0);
                    String number=cursor.getString(1);
                    long id=cursor.getLong(2);
                    contact.put("name", name);
                    contact.put("number", number);
                    contact.put("id", id);
                    array.put(contact);
                }
                cursor.close();

                response(toJson(0, array));
                return;
            }

        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }

        response(toJson(-1, "Get contacts list failed"));
    }
}
