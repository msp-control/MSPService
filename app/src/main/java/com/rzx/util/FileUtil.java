package com.rzx.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/23.
 */
public class FileUtil {

    public static void writeFile(String filepath, String content){
        try {
            File f=new File(filepath);
            if(!f.exists()){
                f.getParentFile().mkdirs();
                f.createNewFile();
            }
            BufferedWriter bw=new BufferedWriter(new FileWriter(f));
            bw.write(content);
            bw.flush();
            bw.close();
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
    }

    public static String readFile(String filepath){
        try {
            File f=new File(filepath);
            if(!f.exists()){
                return "File not found";
            }
            BufferedReader br=new BufferedReader(new FileReader(f));
            String line="";
            StringBuffer sb=new StringBuffer();
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            return sb.toString();
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
        return "Read file error";
    }
}
