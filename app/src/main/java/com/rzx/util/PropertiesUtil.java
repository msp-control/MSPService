package com.rzx.util;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Administrator on 2016/11/14.
 */
public class PropertiesUtil {

    public static void setProperties(String filepath, String key, String value){
        FileInputStream fis=null;
        FileOutputStream fos=null;
        try{
            File f=new File(filepath);
            if(!f.exists()) {
                f.getParentFile().mkdirs();
                f.createNewFile();
            }
            fis=new FileInputStream(f);
            Properties properties=new Properties();
            properties.load(fis);
            properties.setProperty(key, value);
            fos=new FileOutputStream(f);
            properties.store(fos, null);
        }catch (Exception e){
            Log.d("zz", "exception:" + e.toString());
        }finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            }catch (Exception e1){

            }
            try {
                if(fos!=null){
                    fis.close();
                }
            }catch (Exception e1){

            }
        }
    }

    public static String getProperties(String filepath, String key, String defaultValue){
        FileInputStream fis=null;
        try{
            File f=new File(filepath);
            if(!f.exists()) return defaultValue;
            fis=new FileInputStream(f);
            Properties properties=new Properties();
            properties.load(fis);
            String result=properties.getProperty(key);
            if(result==null) result=defaultValue;
            return  result;
        }catch (Exception e){
            Log.d("zz", "exception:" + e.toString());
        }finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            }catch (Exception e1){

            }
        }
        return defaultValue;
    }

    public static void setProperties(String filepath, Map<String, String> params){
        FileInputStream fis=null;
        FileOutputStream fos=null;
        try{
            File f=new File(filepath);
            if(!f.exists()) {
                f.getParentFile().mkdirs();
                f.createNewFile();
            }
            fis=new FileInputStream(f);
            Properties properties=new Properties();
            properties.load(fis);
            if(params!=null){
                for(Map.Entry<String, String> entry : params.entrySet()){
                    properties.setProperty(entry.getKey(), entry.getValue());
                }
            }
            fos=new FileOutputStream(f);
            properties.store(fos, null);
        }catch (Exception e){
            Log.d("msp", "exception:" + e.toString());
        }finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            }catch (Exception e1){

            }
            try {
                if(fos!=null){
                    fis.close();
                }
            }catch (Exception e1){

            }
        }
    }

}
