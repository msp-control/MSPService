package com.rzx.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/4.
 */
public class Agent {

    public static final String VERSION="1.0.32";

    public static void main(String[] args){
        for(String arg : args){
            if(arg.equals("--version")){
                System.out.println(VERSION);
            }

            if(arg.equals("--display")){
                String size=CmdExecutor.executeWithResponse("wm size");
                System.out.println(size);
                String density=CmdExecutor.executeWithResponse("wm density");
                System.out.println(density);
            }

            if(arg.equals("--imei")){
                String imei=FileUtil.readFile("/sdcard/msp/imei.txt");
                System.out.println(imei);
//                try {
//                    File f=new File("/sdcard/msp/imei.txt");
//                    if(!f.exists()){
//                        return ;
//                    }
//                    BufferedReader br=new BufferedReader(new FileReader(f));
//                    String line="";
//                    StringBuffer sb=new StringBuffer();
//                    while((line=br.readLine())!=null){
//                        sb.append(line);
//                    }
//                    System.out.println(sb.toString());
//                }catch (Exception e){
//                    StringWriter sw=new StringWriter();
//                    e.printStackTrace(new PrintWriter(sw));
//                    Log.d("msp", sw.toString());
//                }
            }

        }
    }
}
