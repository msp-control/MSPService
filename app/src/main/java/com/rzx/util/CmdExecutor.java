package com.rzx.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/3.
 */
public class CmdExecutor {

    public static void execute(String cmd){
        try{
            Process process=Runtime.getRuntime().exec("su");
            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            bw.write(cmd);
            bw.write("\n");
            bw.flush();
            bw.close();
            process.waitFor();
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
    }

    public static void execute(String[] cmds){
        try{
            Process process=Runtime.getRuntime().exec("su");
            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            for(String cmd :cmds){
                bw.write(cmd);
                bw.write("\n");
            }
            bw.flush();
            bw.close();
            process.waitFor();
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
    }

    public static String executeWithResponse(String cmd){
        try{
            Process process=Runtime.getRuntime().exec("su");
            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            bw.write(cmd);
            bw.write("\n");
            bw.flush();
            bw.close();
            process.waitFor();
            BufferedReader br=new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuffer sb=new StringBuffer("");
            String line;
            while((line=br.readLine())!=null){
                sb.append(line);
                sb.append("\n");
            }
            if (sb.length() >0 ) {
                sb.deleteCharAt(sb.length() - 1);
            }
            return sb.toString();
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
        return "Execute cmd failed";
    }

}
