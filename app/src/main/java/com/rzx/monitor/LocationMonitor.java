package com.rzx.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.util.Log;

import com.rzx.util.ClassParseUtil;
import com.rzx.util.PropertiesUtil;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2017/2/17.
 */
public class LocationMonitor extends Monitor{

    private Context context;
    private Timer timer;
    private WriteToHookFileTask task;
    private LocationUpdateReceiver receiver;

    public LocationMonitor(Context context){
        this.context=context;
        timer=new Timer();
    }

    @Override
    public void run() {

        receiver=new LocationUpdateReceiver();
        IntentFilter filter=new IntentFilter("com.rzx.mspservice.location.update");
        context.registerReceiver(receiver, filter);

        timer=new Timer();

        try{
            synchronized (this){
                while(!isInterrupted()){
                    wait();
                }
            }
        }catch (Exception e){

        }finally {
            context.unregisterReceiver(receiver);
            if(timer!=null) timer.cancel();
        }

    }

    private class LocationUpdateReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(task!=null){
                task.cancel();
                task=null;
            }
            task=new WriteToHookFileTask();
            timer.schedule(task, 0, 3*60*60*1000);
        }
    }

    private class WriteToHookFileTask extends TimerTask{

        @Override
        public void run() {
            try {
                String positionsStr=context.getSharedPreferences("cache", Context.MODE_PRIVATE).getString("positions", null);
                if(positionsStr!=null){
                    ArrayList<HashMap<String, String>> positions= (ArrayList<HashMap<String, String>>)ClassParseUtil.string2List(positionsStr);
                    int randomIndex=(int)(Math.random()*positions.size());
                    HashMap<String, String> randomPosition=positions.get(randomIndex);
                    PropertiesUtil.setProperties("/sdcard/msp/res/location.properties", randomPosition);
                }
            }catch (Exception e){
                StringWriter sw=new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Log.d("msp", sw.toString());
            }
        }
    }
}
