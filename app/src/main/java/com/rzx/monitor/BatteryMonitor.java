package com.rzx.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by Administrator on 2017/1/23.
 */
public final class BatteryMonitor extends Monitor {

    private Context context;
    private static int level;

    public BatteryMonitor(Context context){
        this.context=context;
    }

    @Override
    public void run() {
        IntentFilter intentFilter=new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        BatteryBroadcastReceiver receiver=new BatteryBroadcastReceiver();
        context.registerReceiver(receiver, intentFilter);

        try{
            synchronized (this){
                while(!isInterrupted()){
                    wait();
                }
            }
        }catch (Exception e){

        }finally {
            context.unregisterReceiver(receiver);
        }

//        while (!isInterrupted()){
//            try{
//                String batteryInfo=CmdExecutor.executeWithResponse("dumpsys battery |grep level");
//                Log.d("msp", "battery:"+batteryInfo);
//                sleep(5000);
//            }catch (Exception e){
//                StringWriter sw=new StringWriter();
//                e.printStackTrace(new PrintWriter(sw));
//                Log.d("msp", sw.toString());
//            }
//        }
    }

    private class BatteryBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals(Intent.ACTION_BATTERY_CHANGED)){
                level=intent.getIntExtra("level", 0);
            }
        }
    }

    public static synchronized int getBattery(){
        return level;
    }
}
