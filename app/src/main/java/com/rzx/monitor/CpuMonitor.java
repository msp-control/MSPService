package com.rzx.monitor;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/7.
 */
public final class CpuMonitor extends Monitor {

    private static int cpuPercent;

    @Override
    public void run() {
        while(!isInterrupted()){
            try {
                Process p = Runtime.getRuntime().exec("top -n 1");
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                while ((line = br.readLine()) != null) {
                    if (line.trim().length() < 1) {
                        continue;
                    } else {
                        String[] cpuUsage = line.split("%");
                        String[] user = cpuUsage[0].split("User");
                        String[] system = cpuUsage[1].split("System");
                        cpuPercent = Integer.parseInt(user[1].trim()) + Integer.parseInt(system[1].trim());
                        break;
                    }
                }
            } catch (Exception e) {
                StringWriter sw=new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Log.d("msp", sw.toString());
            }

            try{
                sleep(5000);
            }catch (InterruptedException e){

            }
        }
    }

    public static synchronized int getCpuPercent(){
        return cpuPercent;
    }

}
