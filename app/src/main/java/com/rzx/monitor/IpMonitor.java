package com.rzx.monitor;

import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2017/2/6.
 */
public final class IpMonitor extends Monitor{

    private static String ip="0.0.0.0";

    public void run() {

        while(!isInterrupted()){
            BufferedReader br=null;
            try{
                InetAddress ipAddr=InetAddress.getByName("whatismyip.akamai.com");
                if(!ipAddr.equals("")){
                    URL url=new URL("http://whatismyip.akamai.com/");
                    HttpURLConnection connection=(HttpURLConnection)url.openConnection();
                    if(connection.getResponseCode()==HttpURLConnection.HTTP_OK){
                        br=new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        StringBuffer sb=new StringBuffer();
                        String line=null;
                        while ((line=br.readLine())!=null){
                            sb.append(line);
                        }
                        if(isIpFormat(sb.toString())){
                            ip=sb.toString();
                        }else{
                            ip="0.0.0.0";
                        }
                    }
                }else{
                    ip="0.0.0.0";
                }
            }catch (UnknownHostException e){
                ip="0.0.0.0";
            }catch (Exception e){
                StringWriter sw=new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Log.d("msp", sw.toString());
            }finally {
                try {
                    if(br!=null){
                        br.close();
                    }
                }catch (Exception e1){

                }
            }
            try{
                sleep(10000);
            }catch (Exception e){

            }
        }
    }

    private boolean isIpFormat(String str){
        Pattern pattern=Pattern.compile("^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$");
        Matcher matcher=pattern.matcher(str);
        if(matcher.matches()) return true;
        return false;
    }

    public static synchronized String getIp(){
        return ip;
    }

}
