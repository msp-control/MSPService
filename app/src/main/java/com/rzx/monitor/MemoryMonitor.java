package com.rzx.monitor;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/2/7.
 */
public final class MemoryMonitor extends Monitor {

    private Context context;
    private static int memPercent;

    public MemoryMonitor(Context context){
        this.context=context;
    }

    @Override
    public void run() {
        while(!isInterrupted()){
            try {
                if(Build.VERSION.SDK_INT>=16){
                    ActivityManager am=(ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
                    ActivityManager.MemoryInfo memInfo=new ActivityManager.MemoryInfo();
                    am.getMemoryInfo(memInfo);
                    memPercent=100-(int)((float)memInfo.availMem/memInfo.totalMem*100);
                }else{
                    Process p = Runtime.getRuntime().exec("dumpsys meminfo |grep RAM");
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    int total=0;
                    int used=0;
                    while ((line = br.readLine()) != null) {
                        if(line.contains("Total")){
                            String[] strs=line.split("RAM:");
                            String[] strs2=strs[1].split("kB");
                            total=Integer.valueOf(strs2[0].trim());
                            continue;
                        }else if(line.contains("Used")){
                            String[] strs=line.split("RAM:");
                            String[] strs2=strs[1].split("kB");
                            used=Integer.valueOf(strs2[0].trim());
                            break;
                        }else{
                            continue;
                        }
                    }
                    memPercent=(int)(used*1.0f/total*100);
                }
            } catch (Exception e) {
                StringWriter sw=new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Log.d("msp", sw.toString());
            }
            try{
                sleep(5000);
            }catch (InterruptedException e){

            }
        }
    }

    public static synchronized int getMemPercent(){
        return memPercent;
    }
}
