package com.rzx.monitor;

/**
 * Created by Administrator on 2017/2/7.
 */
public class StatusMonitor {

    public static final int STATUS_IDLE=1;
    public static final int STATUS_BUSY=2;
    private static int status=1;

    public static void setStatus(int status) {
        StatusMonitor.status = status;
    }

    public static synchronized int getStatus() {
        return status;
    }
}
