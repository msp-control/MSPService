package com.rzx.monitor;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by Administrator on 2017/1/22.
 */
public final class PhoneStateMonitor extends Monitor{

    private Context context;

    private static int strength=99;

    public PhoneStateMonitor(Context context){
        this.context=context;
    }

    @Override
    public void run() {
        TelephonyManager tm=(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        PhoneStateListener listener=new PhoneStateListener(){
            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                strength=signalStrength.getGsmSignalStrength();
            }
        };
        tm.listen(listener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        try{
            synchronized (this){
                while(!isInterrupted()){
                    wait();
                }
            }
        }catch (Exception e){

        }finally {
            tm.listen(listener, PhoneStateListener.LISTEN_NONE);
        }
    }

    public static synchronized int getSignalStrength(){
        return strength;
    }
}
