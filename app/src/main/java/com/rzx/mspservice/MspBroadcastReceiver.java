package com.rzx.mspservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.rzx.util.CmdExecutor;
import com.rzx.util.MD5Encrypt;

/**
 * Created by Administrator on 2017/2/23.
 */
public class MspBroadcastReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        String action=intent.getAction();
        if(action.equals("com.rzx.mspservice.action.VERSION")){
            setResultCode(0);
            setResultData("1.0.29");
        }
        if(action.equals("com.rzx.mspservice.action.ID")){
            TelephonyManager tm=(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            String imei=tm.getDeviceId();
            StringBuffer sb=new StringBuffer("rzx");
            sb.append(Build.SERIAL).append(imei);
            setResultCode(0);
            setResultData(MD5Encrypt.MD5Encode(sb.toString()));
        }
        if(action.equals("com.rzx.mspservice.action.DISPLAY")){
            WindowManager wm=(WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            Display display=wm.getDefaultDisplay();
            setResultCode(0);
            setResultData(display.getWidth()+"x"+display.getHeight());
        }
        if(action.equals("com.rzx.mspservice.action.KILL")){
            Log.d("msp", "kill");
            CmdExecutor.execute("busybox ps -ef|grep 'com.rzx.mspservice'|grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -9");
            android.os.Process.killProcess(Process.myPid());

        }
        if(action.equals("com.rzx.mspservice.action.CMD")){
            String cmd=intent.getStringExtra("cmd");
            String result=null;
            if(cmd!=null){
                result=CmdExecutor.executeWithResponse(cmd);
            }
            if(result!=null){
                setResultCode(0);
                setResultData(result);
            }
        }
    }
}
