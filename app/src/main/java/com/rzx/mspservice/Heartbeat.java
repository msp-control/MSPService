package com.rzx.mspservice;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.rzx.action.Action;
import com.rzx.monitor.BatteryMonitor;
import com.rzx.monitor.CpuMonitor;
import com.rzx.monitor.IpMonitor;
import com.rzx.monitor.MemoryMonitor;
import com.rzx.monitor.PhoneStateMonitor;
import com.rzx.monitor.StatusMonitor;
import com.rzx.util.MD5Encrypt;

import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2017/1/16.
 */
public final class Heartbeat extends Action{

    public Heartbeat(Context context, Connection connection, long taskId, int type){
        super(context, connection, taskId, type);
    }

    @Override
    public void run() {
        try{
            HashMap<String, Object> params=new HashMap<>();

            //硬件信息
            String serial= Build.SERIAL;
            String abi=Build.CPU_ABI;
            String manufacturer=Build.MANUFACTURER;
            String model=Build.MODEL;
            String product=Build.PRODUCT;
            String sdk=String.valueOf(Build.VERSION.SDK_INT);
            String version=Build.VERSION.RELEASE;

            //sim卡序列，imei, imsi, androidId
            TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            String iccid=tm.getSimSerialNumber();
            String imsi=tm.getSubscriberId();
            String imei=tm.getDeviceId();
            String androidId=Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

            //设备识别码
            String identity=context.getSharedPreferences("cache", Context.MODE_PRIVATE).getString("identity", null);
            if(identity==null){
                StringBuffer sb=new StringBuffer("rzx");
                sb.append(serial).append(imei);
                identity= MD5Encrypt.MD5Encode(sb.toString());
                context.getSharedPreferences("cache", Context.MODE_PRIVATE).edit().putString("identity", identity).commit();
            }

            //通信方式
            ConnectivityManager cm=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=cm.getActiveNetworkInfo();
            String networkType;
            if(networkInfo!=null){
                networkType=networkInfo.getTypeName();
            }else{
                networkType="Network Unavailable";
            }

            //mac物理地址
            WifiManager wifiManager=(WifiManager)context.getSystemService(Context.WIFI_SERVICE);
            String mac = wifiManager.getConnectionInfo().getMacAddress();

            //内网ip
            String inIp=getLocalIPAddress();

            //外网ip
            String outIp= IpMonitor.getIp();

            //wifi状态
            int wifiState=wifiManager.getWifiState();

            //飞行模式状态
            int airplaneState= Integer.valueOf(Settings.System.getString(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON));

            //信号强度
            String networkStrength="0";
            if(networkType.toLowerCase().equals("wifi")){
                networkStrength=String.valueOf(WifiManager.calculateSignalLevel(wifiManager.getConnectionInfo().getRssi(), 100));
            }else if(networkType.toLowerCase().equals("mobile")){
                networkStrength=String.valueOf(WifiManager.calculateSignalLevel(PhoneStateMonitor.getSignalStrength(), 100));
            }

            //屏幕亮度
            int brightness=Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);

            //休眠时间
            int screenOffTime = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);

            //cpu使用率
            int cpuPercent= CpuMonitor.getCpuPercent();

            //内存使用率
            int memPercent=MemoryMonitor.getMemPercent();

            //电量
            int battery=0;
            if(Build.VERSION.SDK_INT>=21){
                BatteryManager bm=(BatteryManager)context.getSystemService(Context.BATTERY_SERVICE);
                battery=bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE);
            }else{
                battery= BatteryMonitor.getBattery();
            }

            //状态
            int status= StatusMonitor.getStatus();

            params.put("identityId", identity);
            params.put("serial", serial);
            params.put("abi", abi);
            params.put("manufacturer", manufacturer);
            params.put("model", model);
            params.put("product", product);
            params.put("sdk", sdk);
            params.put("version", version);
            params.put("airplaneModeStatus", airplaneState);
            params.put("brightness", brightness);
            params.put("screenOffTime", screenOffTime);
            params.put("network", networkType);
            params.put("iccid", iccid);
            params.put("imsi", imsi);
            params.put("imei", imei);
            params.put("androidId", androidId);
            params.put("mac", mac);
            params.put("inIp", inIp);
            params.put("outIp", outIp);
            params.put("wifiState", wifiState);
            params.put("cpuPercent", cpuPercent+"%");
            params.put("memPercent", memPercent+"%");
            params.put("battery", battery+"%");
            params.put("signalStrength", networkStrength+"%");
            params.put("status", status);

            JSONObject msgJson=new JSONObject();
            Set<Map.Entry> entries=((Map)params).entrySet();
            for(Map.Entry entry : entries){
                msgJson.put((String)entry.getKey(), entry.getValue());
            }
            response(toJson(0, msgJson));
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
    }

    //获取本地ip
    private String getLocalIPAddress() {
        try {
            for (Enumeration<NetworkInterface> mEnumeration = NetworkInterface.getNetworkInterfaces(); mEnumeration.hasMoreElements();) {
                NetworkInterface intf = mEnumeration.nextElement();
                for (Enumeration<InetAddress> enumIPAddr = intf
                        .getInetAddresses(); enumIPAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIPAddr.nextElement();
                    // 判断是否是ipv4
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        // 直接返回本地IP地址
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());
        }
        return "0.0.0.0";
    }

}
