package com.rzx.mspservice;

import android.app.Service;
import android.content.Intent;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.*;
import android.os.Process;
import android.util.Log;
import com.rzx.monitor.BatteryMonitor;
import com.rzx.monitor.CpuMonitor;
import com.rzx.monitor.IpMonitor;
import com.rzx.monitor.LocationMonitor;
import com.rzx.monitor.MemoryMonitor;
import com.rzx.monitor.PhoneStateMonitor;
import com.rzx.util.CmdExecutor;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2017/1/13.
 */
public class MspService extends Service {

    private MspThread mspThread;
    private ArrayList<Connection> connections;
    private ExecutorService  cacheExecutor;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        Intent intent = new Intent(this, InputService.class);
        startService(intent);

        cacheExecutor=Executors.newCachedThreadPool();
        cacheExecutor.submit(new PhoneStateMonitor(this));
        cacheExecutor.submit(new IpMonitor());
        cacheExecutor.submit(new BatteryMonitor(this));
        cacheExecutor.submit(new CpuMonitor());
        cacheExecutor.submit(new MemoryMonitor(this));
        cacheExecutor.submit(new LocationMonitor(this));

        connections=new ArrayList<>();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(mspThread==null){
            mspThread=new MspThread();
            mspThread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent=new Intent(this, InputService.class);
        stopService(intent);

        for(int i=0; i<connections.size(); i++){
            connections.get(i).stop();
        }
        mspThread.interrupt();
        cacheExecutor.shutdownNow();
        System.gc();
        CmdExecutor.execute("busybox ps -ef|grep 'com.rzx.mspservice'|grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -9");
        android.os.Process.killProcess(Process.myPid());
        Log.d("msp", "onDestroy");
    }

    public void removeConnection(Connection connection){
        connections.remove(connection);
    }

    private class MspThread extends Thread{

        private ServerSocket serverSocket;

        @Override
        public void interrupt() {
            super.interrupt();
            try{
                if(serverSocket!=null){
                    serverSocket.close();
                    Log.d("msp", "close");
                }
            }catch (Exception e){
                StringWriter sw=new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Log.d("msp", sw.toString());
            }

        }

        @Override
        public void run() {

            try{
                serverSocket=new ServerSocket();
                serverSocket.setReuseAddress(true);
                serverSocket.bind(new InetSocketAddress(31218));
                serverSocket.setSoTimeout(10000);
                Log.d("msp", "connecting");
            }catch (Exception e){
                StringWriter sw=new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                Log.d("msp", sw.toString());

                CmdExecutor.execute("busybox ps -ef|grep 'com.rzx.mspservice'|grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -9");
                Process.killProcess(Process.myPid());

                return;
            }

            while(!isInterrupted()){
                try {
                    Socket socket = serverSocket.accept();
                    Log.d("msp", "connected");

                    Connection connection=new Connection(MspService.this, socket);
                    connection.start();
                    connections.add(connection);

                }catch (SocketTimeoutException e){
                    //do nothing
                }catch (IOException e){
                    StringWriter sw=new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));
                    Log.d("msp", sw.toString());
                }
            }

        }
    }

}
