package com.rzx.mspservice;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.security.cert.X509Certificate;

public class MainActivity extends Activity {

    private Button btnStart, btnStop, btnInput, btnInput2;
    private EditText etInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStart=(Button)findViewById(R.id.btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MspService.class);
                startService(intent);

                Intent i = new Intent(MainActivity.this, InputService.class);
                startService(i);
            }
        });

        btnStop=(Button)findViewById(R.id.btn_stop);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, MspService.class);
                stopService(intent);

                Intent i=new Intent(MainActivity.this, InputService.class);
                stopService(i);
            }
        });
//
//        etInput=(EditText)findViewById(R.id.et_input);
//
//        btnInput=(Button)findViewById(R.id.btn_input);
//        btnInput.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent("com.rzx.mspservice.input");
//                i.putExtra("content", "hello world");
//                sendBroadcast(i);
//            }
//        });
//
//        btnInput2=(Button)findViewById(R.id.btn_del);
//        btnInput2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent("com.rzx.mspservice.input");
//                i.putExtra("content", "\b");
//                sendBroadcast(i);
//            }
//        });

        AsyncTask task=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                
                return null;
            }
        };
    }
}
