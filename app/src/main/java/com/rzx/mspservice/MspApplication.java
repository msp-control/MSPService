package com.rzx.mspservice;

import android.app.Application;
import android.telephony.TelephonyManager;

import com.rzx.util.FileUtil;

/**
 * Created by Administrator on 2017/2/23.
 */
public class MspApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        TelephonyManager tm=(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        String imei=tm.getDeviceId();

        FileUtil.writeFile("/sdcard/msp/imei.txt", imei);

    }

}
