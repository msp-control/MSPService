package com.rzx.mspservice;

import android.content.Context;
import android.util.Log;

import com.rzx.action.AirplaneAction;
import com.rzx.action.BrightnessAction;
import com.rzx.action.Action;
import com.rzx.action.ClearCacheAction;
import com.rzx.action.ClearContactsAction;
import com.rzx.action.ClearPicturesAction;
import com.rzx.action.DeleteContactAction;
import com.rzx.action.GetContactsListAction;
import com.rzx.action.GetPicturesAction;
import com.rzx.action.ImportContactsExAction;
import com.rzx.action.InputAction;
import com.rzx.action.InputBackspaceAction;
import com.rzx.action.InputEnterAction;
import com.rzx.action.OpenMsgEditorAction;
import com.rzx.action.RefreshPicturesAction;
import com.rzx.action.SetLocationAction;
import com.rzx.action.StopAppsAction;
import com.rzx.action.ErrorAction;
import com.rzx.action.ImportContactsAction;
import com.rzx.action.InstallAction;
import com.rzx.action.KeyCodeBackAction;
import com.rzx.action.KeyCodeHomeAction;
import com.rzx.action.KeyCodeMenuAction;
import com.rzx.action.OpenMsgAction;
import com.rzx.action.PackageInfoAction;
import com.rzx.action.PowerAction;
import com.rzx.action.RebootAction;
import com.rzx.action.ShutdownAction;
import com.rzx.action.SuAction;
import com.rzx.action.UninstallAction;
import com.rzx.action.WifiAction;
import com.rzx.action.WriteInfoAction;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Administrator on 2017/1/13.
 */
public class Dispacher {

    public static Action handle(Context context, Connection connection, String request){
        Action action =null;
        long taskId=0;
        int type=0;
        try {
            JSONObject jsonObject=new JSONObject(request);
            taskId=jsonObject.getLong("id");
            type=jsonObject.getInt("type");
            Object args=null;
            if(jsonObject.has("args")){
                args=jsonObject.get("args");
            }

            //业务逻辑判断并处理返回action
            if(type==1){
                //心跳
                action = new Heartbeat(context, connection, taskId, type);
            }else  if(type==1000){
                //wifi
                boolean enable=((JSONObject)args).getBoolean("enable");
                action = new WifiAction(context, connection, taskId, type, enable);
            }else if(type==1001){
                //飞行模式
                boolean enable=((JSONObject)args).getBoolean("enable");
                action = new AirplaneAction(context, connection, taskId, type, enable);
            }else if(type==1002){
                //亮熄屏
                boolean enable=((JSONObject)args).getBoolean("enable");
                action = new PowerAction(context, connection, taskId, type, enable);
            }else if(type==1003){
                //gps
                String location=((JSONObject)args).getString("name");
                JSONArray positions=((JSONObject)args).getJSONArray("positions");
                action = new SetLocationAction(context, connection, taskId, type, location, positions);
            }else if(type==1004){
                //导入图片后刷新
                JSONArray files=((JSONObject)args).getJSONArray("files");
                action = new RefreshPicturesAction(context, connection, taskId, type, files);
            }else if(type==1005){
                //导入通讯录
                String filepath=((JSONObject)args).getString("file");
                action = new ImportContactsAction(context, connection, taskId, type, filepath);
            }else if(type==1006){
                //清空通讯录
                action = new ClearContactsAction(context, connection, taskId, type);
            }else if(type==1007){
                //清空相册
                action = new ClearPicturesAction(context, connection, taskId, type);
            }else if(type==1008){
                //重启
                action = new RebootAction(context, connection, taskId, type);
            }else if(type==1009){
                //关机
                action = new ShutdownAction(context, connection, taskId, type);
            }else if(type==1010){
                //亮度
                int brightness=((JSONObject)args).getInt("brightness");
                action = new BrightnessAction(context, connection, taskId, type, brightness);
            }else if(type==1011){
                //安装APK
                String filepath=((JSONObject)args).getString("file");
                action = new InstallAction(context, connection, taskId, type, filepath);
            }else if(type==1012){
                //卸载APK
                String packagename=((JSONObject)args).getString("pkg");
                action = new UninstallAction(context, connection, taskId, type, packagename);
            }else if(type==1015){
                //home键
                action = new KeyCodeHomeAction(context, connection, taskId, type);
            }else if(type==1016){
                //返回键
                action = new KeyCodeBackAction(context, connection, taskId, type);
            }else if(type==1017){
                //菜单键
                action = new KeyCodeMenuAction(context, connection, taskId, type);
            }else if(type==1018){
                //获取所有程序信息
                action = new PackageInfoAction(context, connection, taskId, type);
            }else if(type==1019){
                //获取联系人列表
                action = new GetContactsListAction(context, connection, taskId, type);
            }else if(type==1020){
                //清除所有三方应用缓存
                action = new ClearCacheAction(context, connection, taskId, type);
            }else if(type==1021){
                //退出所有三方应用
                action = new StopAppsAction(context, connection, taskId, type);
            }else if(type==1022){
                //打开短信栏
                action = new OpenMsgAction(context, connection, taskId, type);
            }else if(type==1023){
                //打开短信发送页面
                action = new OpenMsgEditorAction(context, connection, taskId, type);
            }else if(type==1024){
                //以非文本方式导入联系人
                action = new ImportContactsExAction(context, connection, taskId, type, (JSONArray)args);
            }else if(type==1025){
                //根据id删除联系人
                long contactId=((JSONObject)args).getInt("contactId");
                action = new DeleteContactAction(context, connection, taskId, type, contactId);
            }else if(type==1026){
                //写设备信息到文件
                action = new WriteInfoAction(context, connection, taskId, type, (JSONObject)args);
            }else if(type==1027){
                //获取相册信息
                action = new GetPicturesAction(context, connection, taskId, type);
            }else if(type==1028){
                //执行su命令
                String cmd=((JSONObject)args).getString("cmd");
                action = new SuAction(context, connection, taskId, type, cmd);
            }else if(type==3000){
                //输入字符
                String text=((JSONObject)args).getString("char");
                action = new InputAction(context, connection, taskId, type, text);
            }else if(type==3001){
                //输入回车
                action = new InputEnterAction(context, connection, taskId, type);
            }else if(type==3002){
                //输入回退
                action = new InputBackspaceAction(context, connection, taskId, type);
            }
        }catch (Exception e){
            StringWriter sw=new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.d("msp", sw.toString());

            action = new ErrorAction(context, connection, taskId, type);
        }
        return action;
    }
}
