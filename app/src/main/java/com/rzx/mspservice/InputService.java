package com.rzx.mspservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.inputmethodservice.InputMethodService;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.EditText;

import com.rzx.mspservice.R;
import com.rzx.util.CmdExecutor;

/**
 * Created by Administrator on 2017/2/20.
 */
public class InputService extends InputMethodService{

    private InputBroadcastReceiver receiver;

    @Override
    public void onCreate() {
        super.onCreate();
        receiver=new InputBroadcastReceiver();
        IntentFilter filter=new IntentFilter("com.rzx.mspservice.input");
        registerReceiver(receiver, filter);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CmdExecutor.execute("ime enable com.rzx.mspservice/.InputService");
                CmdExecutor.execute("ime set com.rzx.mspservice/.InputService");
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private class InputBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String content=intent.getStringExtra("content");
            InputConnection connection=getCurrentInputConnection();
            if(connection!=null){
                if(content.equals("\b")){
                    sendDownUpKeyEvents(KeyEvent.KEYCODE_DEL);
                }else if(content.equals("\n")){
                    sendKeyChar('\n');
                }else{
                    connection.commitText(content, 0);
                }
            }else{
                Log.d("msp", "Input connection is null");
            }
        }
    }
}
